package ${package}.adapter.inbound;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import ${package}.port.inbound.PortInbound;
import ${package}.domain.Sample;

@RestController
@RequestMapping("/sample")
public class AdapterSampleRest {
    private static final Logger log = LoggerFactory.getLogger(AdapterSampleRest.class);
    
    @Autowired
    private PortInbound core;

    @GetMapping
    @ApiOperation(value = "Find all", notes = "This method looking for every record on database",
        response = Sample.class, responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Record found", responseContainer = "List", response = Sample.class),
		@ApiResponse(code = 400, message = "Bad Request"),
		@ApiResponse(code = 401, message = "Not Authorized"),
		@ApiResponse(code = 404, message = "Not Founded"),
		@ApiResponse(code = 500, message = "Internal Server Error")})
    public ResponseEntity<List<Sample>> findAll() {
        log.debug("requesting findAll service");
        return new ResponseEntity<List<Sample>>(core.findAll(), HttpStatus.OK);
    }


    @GetMapping("/{id}")
    @ApiOperation(value = "Find a record by Id", notes = "This method will search any record who matchs with Id",
        response = Sample.class)
    @ApiResponses(value = {
    		@ApiResponse(code = 200, message = "Record found", response = Sample.class),
    		@ApiResponse(code = 400, message = "Bad Request"),
    		@ApiResponse(code = 401, message = "Not Authorized"),
    		@ApiResponse(code = 404, message = "Not Founded"),
    		@ApiResponse(code = 500, message = "Internal Server Error")})
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "Id to find", required = true, dataType = "String",
        paramType = "query")})
    public ResponseEntity<Sample> findOne(@PathVariable("id") String id) {
        log.debug("requesting findOne service");
        final Sample sample = core.findOne(id);
        if (null == sample) {
            return new ResponseEntity<Sample>(sample, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<Sample>(sample, HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    @ApiOperation(value = "Update an record", notes = "Update a entity base on an object passed by parameter",
        response = Sample.class)
    @ApiResponses(value = {
    		@ApiResponse(code = 200, message = "Record updated"),
    		@ApiResponse(code = 400, message = "Bad Request"),
    		@ApiResponse(code = 401, message = "Not Authorized"),
    		@ApiResponse(code = 404, message = "Not Founded"),
    		@ApiResponse(code = 500, message = "Internal Server Error")})
    @ApiImplicitParams({
        @ApiImplicitParam(name = "Sample", value = "Sample to update", required = true, dataType = "Sample")})
    public ResponseEntity<Sample> update(@RequestBody @Valid Sample sample, @Valid @NotNull @PathVariable("id") String id) {
        log.debug("requesting update service");

        if (!id.equals(sample.getId()))
    		return new ResponseEntity<Sample>(HttpStatus.BAD_REQUEST);
        
        if (core.findOne(sample.getId()) == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Sample>(core.save(sample), HttpStatus.OK);
    }


    @PostMapping
    @ApiOperation(value = "Create an Sample", notes = "Create a entity base on an object passed by parameter",
        response = Sample.class)
    @ApiResponses(value = {
    		@ApiResponse(code = 200, message = "Sample saved"),
    		@ApiResponse(code = 400, message = "Bad Request"),
    		@ApiResponse(code = 401, message = "Not Authorized"),
    		@ApiResponse(code = 404, message = "Not Founded"),
    		@ApiResponse(code = 500, message = "Internal Server Error")})
    @ApiImplicitParams({
        @ApiImplicitParam(name = "Sample", value = "Sample to insert", required = true, dataType = "Sample")})
    public ResponseEntity<Sample> create(@RequestBody @Valid Sample sample) {
        log.debug("requesting save service");
        return new ResponseEntity<Sample>(core.save(sample), HttpStatus.CREATED);
    }


    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete record by id", notes = "You can delete one item per time", response = Sample.class,
        responseContainer = "List")
    @ApiResponses(value = {
    		@ApiResponse(code = 200, message = "Record deleted"),
    		@ApiResponse(code = 400, message = "Bad Request"),
    		@ApiResponse(code = 401, message = "Not Authorized"),
    		@ApiResponse(code = 404, message = "Not Founded"),
    		@ApiResponse(code = 500, message = "Internal Server Error")})
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "Id to find", required = true, dataType = "String")})
    public ResponseEntity<Void> delete(@PathVariable("id") String id) {
        log.debug("requesting delete service");
        final Sample sampleToDelete = core.findOne(id);
        if (null == sampleToDelete) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        core.delete(sampleToDelete.getId());
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
